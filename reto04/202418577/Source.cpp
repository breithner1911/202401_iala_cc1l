#include <iostream>
#include <vector>
#include "upc.h"

using namespace std;

void generarArreglo(vector<int>& arreglo) {
    srand(time(nullptr));

    int tamano = randint(100, 501);

    for (int i = 0; i < tamano; ++i) {
        arreglo.push_back(randint(1, 10001));
    }
}

int main() {
    vector<int> arreglo;
    generarArreglo(arreglo);

   cout << "Arreglo :" << endl;
    for (int i = 0; i < arreglo.size(); ++i) {
        cout << arreglo[i] << " ";
        if ((i + 1) % 20 == 0)
        cout << endl;
    }
    cout << endl;

    system("pause");
    return EXIT_SUCCESS;
}

